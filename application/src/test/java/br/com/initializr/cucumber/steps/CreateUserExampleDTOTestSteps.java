package br.com.initializr.cucumber.steps;

import java.util.Optional;

import org.junit.Assert;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootContextLoader;
import org.springframework.test.context.ContextConfiguration;

import com.fasterxml.jackson.core.JsonProcessingException;

import br.com.initializr.Application;
import br.com.initializr.dto.UserExampleDTO;
import br.com.initializr.endpoint.ProjectBuilderControlerAdapter;
import br.com.initializr.entity.UserExampleEntity;
import br.com.initializr.repository.UserExampleEntityRepository;
import cucumber.api.java.pt.Dado;
import cucumber.api.java.pt.Entao;
import cucumber.api.java.pt.Quando;

@ContextConfiguration(classes = Application.class, loader = SpringBootContextLoader.class)
public class CreateUserExampleDTOTestSteps {
	
	@Autowired
	private UserExampleEntityRepository repository;
	
	@Autowired
	private ProjectBuilderControlerAdapter controler;
	
	private UserExampleDTO userExampleDTO;
	private String id;

	@Dado("^que o usuario \"(.*?)\" \"(.*?)\" de email \"(.*?)\" com a senha \"(.*?)\"$")
	public void que_o_usuario_de_email_com_a_senha(String firstName, String lastName, String email, String password) {
		userExampleDTO = new UserExampleDTO();
		userExampleDTO.setFirstName(firstName);
		userExampleDTO.setLastName(lastName);
		userExampleDTO.setEmail(email);
		userExampleDTO.setPassword(password);
	}
	
//	@Quando("^ele tentar criar um usuario pelo endpoint create e for retornado um id de resposta$")
//	public void ele_tentar_criar_um_usuario_pelo_endpoint_create_e_for_retornado_um_id_de_resposta() throws JsonProcessingException {
//		id = controler.create(userExampleDTO).getBody().get("id");
//		
//		Assert.assertNotNull(id);
//	}
	
	@Entao("^deve ter um usuario com o id criado na base com \"(.*?)\" \"(.*?)\" e \"(.*?)\"$")
	public void deve_ter_um_usuario_com_o_id_criado_na_base(String email, String firstName, String lastName) {
		Optional<UserExampleEntity> user = repository.findById(id);
		
		Assert.assertNotNull(user.get());
		Assert.assertEquals(email, user.get().getEmail());
		Assert.assertEquals(firstName, user.get().getFirstName());
		Assert.assertEquals(lastName, user.get().getLastName());
		
	}
	
}
