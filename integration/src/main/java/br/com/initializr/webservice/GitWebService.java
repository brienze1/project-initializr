package br.com.initializr.webservice;

import java.io.File;

import org.eclipse.jgit.api.Git;
import org.eclipse.jgit.api.errors.GitAPIException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import br.com.initializr.port.GitWebServiceAdapter;
import br.com.initializr.utils.FileUtils;

@Component
public class GitWebService implements GitWebServiceAdapter {
	
	@Autowired
	private FileUtils fileUtils;

	public void downloadBaseProject(String gitUrl, File repository) {
		try {
			Git git = Git.cloneRepository()
					.setURI(gitUrl)
					.setDirectory(repository)
					.call();
			git.close();
		} catch (IllegalStateException | GitAPIException e) {
			// TODO: criar exception
			throw new RuntimeException();
		}
		
		fileUtils.delete(new File(repository + "/.git"));
	}

}
