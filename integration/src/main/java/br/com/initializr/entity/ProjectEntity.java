package br.com.initializr.entity;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.validation.constraints.Email;

@Entity
@Table(name="TB_BUILDER_PROJECT")
public class ProjectEntity {

	@Id 
	@Column(name="PROJECT_ID")
	private String id;
	
	@Column(name="first_name")
	private String firstName;
	
	@Column(name="last_name")
	private String lastName;

	@Column(name="email")
	private String email;

	@Column(name="personal_website")
	private String personalWebsite;
	
	@Column(name="project_name")
	private String projectName;
	
	@Column(name="description")
	private String decription;
	
	@Column(name="base_package_name")
	private String basePackageName;
	
	@Column(name="base_project_url")
	private String baseProjectUrl;

	@OneToOne(cascade = {CascadeType.ALL})
	@JoinColumn(name = "CONFIG_ID")
	private OptionalConfigurationEntity optionalConfiguration;

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getPersonalWebsite() {
		return personalWebsite;
	}

	public void setPersonalWebsite(String personalWebsite) {
		this.personalWebsite = personalWebsite;
	}

	public String getProjectName() {
		return projectName;
	}

	public void setProjectName(String projectName) {
		this.projectName = projectName;
	}

	public String getDecription() {
		return decription;
	}

	public void setDecription(String decription) {
		this.decription = decription;
	}

	public String getBasePackageName() {
		return basePackageName;
	}

	public void setBasePackageName(String basePackageName) {
		this.basePackageName = basePackageName;
	}

	public String getBaseProjectUrl() {
		return baseProjectUrl;
	}

	public void setBaseProjectUrl(String baseProjectUrl) {
		this.baseProjectUrl = baseProjectUrl;
	}

	public OptionalConfigurationEntity getOptionalConfiguration() {
		return optionalConfiguration;
	}

	public void setOptionalConfiguration(OptionalConfigurationEntity optionalConfiguration) {
		this.optionalConfiguration = optionalConfiguration;
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("ProjectEntity [id=");
		builder.append(id);
		builder.append(", firstName=");
		builder.append(firstName);
		builder.append(", lastName=");
		builder.append(lastName);
		builder.append(", email=");
		builder.append(email);
		builder.append(", personalWebsite=");
		builder.append(personalWebsite);
		builder.append(", projectName=");
		builder.append(projectName);
		builder.append(", decription=");
		builder.append(decription);
		builder.append(", basePackageName=");
		builder.append(basePackageName);
		builder.append(", baseProjectUrl=");
		builder.append(baseProjectUrl);
		builder.append(", optionalConfiguration=");
		builder.append(optionalConfiguration);
		builder.append("]");
		return builder.toString();
	}
	
}
