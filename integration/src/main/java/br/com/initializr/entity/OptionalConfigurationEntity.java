package br.com.initializr.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="TB_BUILDER_CONFIGURATION")
public class OptionalConfigurationEntity {
	
	@Id
	@Column(name="CONFIG_ID")
	private String id;
	
	@Column(name="swagger")
	private boolean swagger;
	
	@Column(name="rest_template")
	private boolean restTemplate;
	
	@Column(name="data_base")
	private boolean dataBase;

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public boolean getSwagger() {
		return swagger;
	}

	public void setSwagger(boolean swagger) {
		this.swagger = swagger;
	}

	public boolean getRestTemplate() {
		return restTemplate;
	}

	public void setRestTemplate(boolean restTemplate) {
		this.restTemplate = restTemplate;
	}

	public boolean getDataBase() {
		return dataBase;
	}

	public void setDataBase(boolean dataBase) {
		this.dataBase = dataBase;
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("OptionalConfigurationEntity [id=");
		builder.append(id);
		builder.append(", swagger=");
		builder.append(swagger);
		builder.append(", restTemplate=");
		builder.append(restTemplate);
		builder.append(", dataBase=");
		builder.append(dataBase);
		builder.append("]");
		return builder.toString();
	}

}
