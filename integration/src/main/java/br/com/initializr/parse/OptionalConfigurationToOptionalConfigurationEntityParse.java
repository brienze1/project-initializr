package br.com.initializr.parse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import br.com.initializr.entity.OptionalConfiguration;
import br.com.initializr.entity.OptionalConfigurationEntity;
import br.com.initializr.utils.UuidGenerator;

@Component
public class OptionalConfigurationToOptionalConfigurationEntityParse {

	@Autowired
	private UuidGenerator uuidGenerator;

	public OptionalConfigurationEntity parse(OptionalConfiguration optionalConfiguration) {
		OptionalConfigurationEntity optionalConfigurationEntity = new OptionalConfigurationEntity();
		
		if(optionalConfiguration != null) {
			optionalConfigurationEntity.setDataBase(optionalConfiguration.getDataBase());
			optionalConfigurationEntity.setRestTemplate(optionalConfiguration.getRestTemplate());
			optionalConfigurationEntity.setSwagger(optionalConfiguration.getSwagger());
			optionalConfigurationEntity.setId(uuidGenerator.generate());
		}
		
		return optionalConfigurationEntity;
	}

}
