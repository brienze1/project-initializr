package br.com.initializr.parse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import br.com.initializr.entity.Project;
import br.com.initializr.entity.ProjectEntity;
import br.com.initializr.utils.UuidGenerator;

@Component
public class ProjectToProjectEntityParse {
	
	@Autowired
	private OptionalConfigurationToOptionalConfigurationEntityParse optionalConfigurationToOptionalConfigurationEntityParse;
	
	@Autowired
	private UuidGenerator uuidGenerator;

	public ProjectEntity parse(Project project) {
		ProjectEntity projectEntity = new ProjectEntity();

		if(project != null) {
			projectEntity.setBasePackageName(project.getBasePackageName());
			projectEntity.setBaseProjectUrl(project.getBaseProjectUrl());
			projectEntity.setDecription(project.getDecription());
			projectEntity.setEmail(project.getEmail());
			projectEntity.setFirstName(project.getFirstName());
			projectEntity.setLastName(project.getLastName());
			projectEntity.setPersonalWebsite(project.getPersonalWebsite());
			projectEntity.setProjectName(project.getProjectName());
			projectEntity.setId(uuidGenerator.generate());
			projectEntity.setOptionalConfiguration(optionalConfigurationToOptionalConfigurationEntityParse.parse(project.getOptionalConfiguration()));
		}

		return projectEntity;
	}
	
}
