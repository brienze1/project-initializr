package br.com.initializr.persistence;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import br.com.initializr.entity.Project;
import br.com.initializr.entity.ProjectEntity;
import br.com.initializr.parse.ProjectToProjectEntityParse;
import br.com.initializr.port.ProjectRepositoryAdapter;
import br.com.initializr.repository.ProjectEntityRepository;

@Component
public class ProjectPersistence implements ProjectRepositoryAdapter {
	
	@Autowired
	private ProjectToProjectEntityParse projectToProjectEntityParse;
	
	@Autowired
	private ProjectEntityRepository projectEntityRepository;

	public void save(Project project) {
		ProjectEntity projectEntity = projectToProjectEntityParse.parse(project);
		
		projectEntityRepository.save(projectEntity);
	}
	
}
