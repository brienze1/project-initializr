package br.com.initializr.utils;

import java.util.UUID;

import org.springframework.stereotype.Component;

import br.com.initializr.port.IdGeneratorAdapter;

@Component
public class UuidGenerator implements IdGeneratorAdapter {

	@Override
	public String generate() {
		return UUID.randomUUID().toString();
	}
}
