package br.com.initializr.utils;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import org.springframework.stereotype.Component;

import br.com.initializr.port.FileUtilsAdapter;

@Component
public class FileUtils implements FileUtilsAdapter {

	public File createFile(String path) {
		File file = new File(path);

		if (file.exists()) {
			delete(file);
		}
		if (file.mkdirs()) {
			return file;
		} else {
			// TODO: criar exception
			throw new RuntimeException();
		}
	}

	@Override
	public File createRepository(String destination) {
		File file = new File(destination);
		if (file.exists()) {
			delete(file);
		}
		if (file.mkdirs()) {
			return file;
		} else {
			// TODO: criar exception
			throw new RuntimeException();
		}
	}

	@Override
	public String[] listAll(File fileDir) {
		File[] files = fileDir.listFiles();

		List<String> fileNames = new ArrayList<>();
		if (files != null) {
			for (File file : files) {
				if (file.isDirectory()) {
					fileNames.addAll(Arrays.asList(listAll(file)));
				} else {
					fileNames.add(file.getName());
				}
			}
		}

		return fileNames.toArray(new String[0]);
	}

	public void renameFolder(File source, String newName) {
		File destFile = new File(source.getPath() + "/" + newName);

		if (!source.renameTo(destFile)) {
			// TODO criar exception
			throw new RuntimeException();
		}
	}

	@Override
	public void clean(File fileDir, String[] cleanFiles, String[] deleteFiles, List<String> start, List<String> stop) {
		File[] files = fileDir.listFiles();

		if (files != null) {
			for (File file : files) {
				if (file.isDirectory()) {
					clean(file, cleanFiles, deleteFiles, start, stop);
				} else {
					for (String fileName : cleanFiles) {
						if (file.getName().equals(fileName)) {
							clean(file, start, stop);
						}
					}
					for (String fileName : deleteFiles) {
						if (file.getName().equals(fileName)) {
							delete(file);
						}
					}
				}
			}
		}
	}

	@Override
	public void delete(File fileToDelete) {
		if (fileToDelete.isFile()) {
			if (!fileToDelete.delete()) {
				// TODO: criar exception
				throw new RuntimeException();
			}
			return;
		}

		File[] files = fileToDelete.listFiles();

		if (files != null) {
			for (File file : files) {
				if (file.isDirectory()) {
					delete(file);
				} else {
					if (!file.delete()) {
						// TODO: criar exception
						throw new RuntimeException();
					}
				}
			}
		}

		if (!fileToDelete.delete()) {
			// TODO: criar exception
			throw new RuntimeException();
		}
	}

	@Override
	public String[] listAllRepositories(File fileRepository) {
		File[] files = fileRepository.listFiles();

		List<String> fileNames = new ArrayList<>();
		if (files != null) {
			for (File file : files) {
				if (file.isDirectory()) {
					fileNames.add(file.getName());
					fileNames.addAll(Arrays.asList(listAllRepositories(file)));
				}
			}
		}

		return fileNames.toArray(new String[0]);

	}

	@Override
	public void copyFiles(File source, File destination) {
		try {
			org.apache.commons.io.FileUtils.copyDirectory(source, destination);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			throw new RuntimeException();
		}
		
		
	}

	@Override
	public void replaceDefaultNames(File fileDir, Map<String, String> map) {
		File[] files = fileDir.listFiles();

		if (files != null) {
			for (File file : files) {
				if (file.isDirectory()) {
					replaceDefaultNames(file, map);
				} else {
					clean(file, map);
				}
			}
		}
	}
	
	private void clean(File file, Map<String, String> map) {
		File newFile = new File(file.getAbsolutePath() + ".tmp");

		try {
			BufferedReader reader = new BufferedReader(new FileReader(file));
			BufferedWriter writer = new BufferedWriter(new FileWriter(newFile));

			String line = null;
			String previousLine = "";
			boolean copy = true;
			while ((line = reader.readLine()) != null) {
				for (Entry<String, String> entry : map.entrySet()) {
					if (line.contains(entry.getKey())) {
						line = line.replaceAll(entry.getKey(), entry.getValue());
						if(entry.getValue().isBlank()) {
							copy = false;
						}
					}
				}
				
				if(line.isBlank() && previousLine.isBlank()) {
					copy = false;
				}
				
				if(copy) {
					writer.write(line + System.getProperty("line.separator"));
				}
				
				previousLine = line;
				copy = true;
			}

			writer.close();
			reader.close();
		} catch (FileNotFoundException e) {
			// TODO criar exception
			throw new RuntimeException();
		} catch (IOException e) {
			// TODO criar exception
			throw new RuntimeException();
		}

		delete(file);

		if (!newFile.renameTo(file)) {
			// TODO criar exception
			throw new RuntimeException();
		}		
	}
	
	private void clean(File file, List<String> start, List<String> stop) {
		File newFile = new File(file.getAbsolutePath() + ".tmp");

		try {
			BufferedReader reader = new BufferedReader(new FileReader(file));
			BufferedWriter writer = new BufferedWriter(new FileWriter(newFile));

			String line = null;
			boolean copy = true;
			while ((line = reader.readLine()) != null) {
				for (String startString : start) {
					if (line.contains(startString)) {
						copy = false;
					}
				}

				if (copy) {
					writer.write(line + System.getProperty("line.separator"));
				}

				for (String stopString : stop) {
					if (line.contains(stopString)) {
						copy = true;
					}
				}
			}

			writer.close();
			reader.close();
		} catch (FileNotFoundException e) {
			// TODO criar exception
			throw new RuntimeException();
		} catch (IOException e) {
			// TODO criar exception
			throw new RuntimeException();
		}

		delete(file);

		if (!newFile.renameTo(file)) {
			// TODO criar exception
			throw new RuntimeException();
		}
	}


}
