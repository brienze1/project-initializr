package br.com.initializr.utils;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;

import org.springframework.stereotype.Component;

import br.com.initializr.port.ZipUtilityAdapter;

@Component
public class ZipUtility implements ZipUtilityAdapter {
	
	private static final int BUFFER_SIZE = 4096;

	public File zip(File file, String destZipFile) {
		ZipOutputStream zos;
		try {
			zos = new ZipOutputStream(new FileOutputStream(destZipFile));
			if (file.isDirectory()) {
				zipDirectory(file, file.getName(), zos);
			} else {
				zipFile(file, zos);
			}
			zos.flush();
			zos.close();
		} catch (FileNotFoundException e) {
			// TODO criar exception
			throw new RuntimeException();
		} catch (IOException e) {
			// TODO criar exception
			throw new RuntimeException();
		}
		return new File(destZipFile);
	}

	private void zipDirectory(File folder, String parentFolder, ZipOutputStream zos) {
		for (File file : folder.listFiles()) {
			if (file.isDirectory()) {
				zipDirectory(file, parentFolder + "/" + file.getName(), zos);
				continue;
			}

			try {
				zos.putNextEntry(new ZipEntry(parentFolder + "/" + file.getName()));
				BufferedInputStream bis = new BufferedInputStream(new FileInputStream(file));
				byte[] bytesIn = new byte[BUFFER_SIZE];
				int read = 0;
				while ((read = bis.read(bytesIn)) != -1) {
					zos.write(bytesIn, 0, read);
				}
				bis.close();
				zos.closeEntry();
			} catch (IOException e) {
				// TODO criar exception
				throw new RuntimeException();
			}
		}
	}

	private void zipFile(File file, ZipOutputStream zos) {
		try {
			zos.putNextEntry(new ZipEntry(file.getName()));
			BufferedInputStream bis = new BufferedInputStream(new FileInputStream(file));

			byte[] bytesIn = new byte[BUFFER_SIZE];
			int read = 0;
			while ((read = bis.read(bytesIn)) != -1) {
				zos.write(bytesIn, 0, read);
			}

			bis.close();
			zos.closeEntry();
		} catch (IOException e) {
			// TODO criar exception
			throw new RuntimeException();
		}
	}
}