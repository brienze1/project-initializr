package br.com.initializr.repository;

import org.springframework.data.repository.CrudRepository;

import br.com.initializr.entity.ProjectEntity;

public interface ProjectEntityRepository extends CrudRepository<ProjectEntity, String>  {

}
