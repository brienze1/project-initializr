package br.com.initializr.endpoint;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.time.LocalDateTime;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.server.ResponseStatusException;

import com.fasterxml.jackson.core.JsonProcessingException;

import br.com.initializr.dto.ProjectDto;
import br.com.initializr.dto.UserExampleResponseDTO;
import br.com.initializr.entity.UserExample;
import br.com.initializr.parse.ProjectDtoToProjectParse;
import br.com.initializr.parse.UserExampleDTOParaUserExampleParse;
import br.com.initializr.parse.UserExampleParaUserExampleResponseDTOParse;
import br.com.initializr.port.ProjectBuilderServiceAdapter;
import br.com.initializr.port.UserServiceAdapter;

@RestController("/initial")
public class ProjectBuilderControler implements ProjectBuilderControlerAdapter {
	
	@Autowired
	private UserExampleDTOParaUserExampleParse userExampleDTOParaUserExampleParse;
	
	@Autowired
	private UserExampleParaUserExampleResponseDTOParse userExampleParaUserExampleResponseDTOParse;
	
	@Autowired
	private UserServiceAdapter userServiceAdapter;
	
	@Autowired
	private ProjectDtoToProjectParse projectDtoToProjectParse;
	
	@Autowired
	private ProjectBuilderServiceAdapter projectBuilderServiceAdapter;
	
	@GetMapping("/find/{id}")
	public @ResponseBody ResponseEntity<UserExampleResponseDTO> findById(@PathVariable(name="id", required=true) String id) throws JsonProcessingException  {
		System.out.println("Chamada (/teste/find/{id}) no horario: " + LocalDateTime.now());
		
		Optional<UserExample> userExample = userServiceAdapter.findById(id);
		
		if(userExample.isPresent()) {
			return new ResponseEntity<UserExampleResponseDTO>(userExampleParaUserExampleResponseDTOParse.parse(userExample.get()), HttpStatus.OK);
		} else {
			throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "User Not Found.");
		}
	    
	}
	
	@GetMapping("/find")
	public @ResponseBody ResponseEntity<List<UserExampleResponseDTO>> findAllUsers() throws JsonProcessingException  {
		System.out.println("Chamada (/teste/find) no horario: " + LocalDateTime.now());
		
		List<UserExample> userExampleList = userServiceAdapter.findAllUsers();
		
		return new ResponseEntity<List<UserExampleResponseDTO>>(userExampleParaUserExampleResponseDTOParse.parseList(userExampleList), HttpStatus.OK);
	}
	
	@ResponseBody
	@PostMapping(path = "/build", produces="application/zip")
	public ResponseEntity<byte[]> build(@RequestBody ProjectDto projectDto) throws IOException {
		System.out.println("Chamada (/teste/post) no horario: " + LocalDateTime.now());
		
		File response = projectBuilderServiceAdapter.build(projectDtoToProjectParse.parse(projectDto));
		
		HttpHeaders headers = new HttpHeaders();
        headers.add("Cache-Control", "no-cache, no-store, must-revalidate");
        headers.add("Content-Type", "application/zip");
        headers.add("Content-Disposition", "attachment; filename=\"" + response.getName() + "\"");
        headers.add("Pragma", "no-cache");
        headers.add("Expires", "0");
			
		return ResponseEntity.ok()
				.headers(headers)
				.contentLength(response.length())
				.contentType(MediaType.parseMediaType(MediaType.APPLICATION_OCTET_STREAM_VALUE))
		        .body(Files.readAllBytes(Paths.get(response.getAbsolutePath())));
	}

}










