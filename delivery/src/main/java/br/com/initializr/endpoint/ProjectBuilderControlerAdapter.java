package br.com.initializr.endpoint;

import java.io.IOException;
import java.util.List;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.ResponseBody;

import com.fasterxml.jackson.core.JsonProcessingException;

import br.com.initializr.dto.ProjectDto;
import br.com.initializr.dto.UserExampleResponseDTO;
import io.swagger.annotations.Api;

@Api
public interface ProjectBuilderControlerAdapter {
	
	@ResponseBody
	@GetMapping(path="/teste/find/{id}", produces="application/json")
	public ResponseEntity<UserExampleResponseDTO> findById(@PathVariable(name="id", required=true) String id) throws JsonProcessingException;
	
	@ResponseBody
	@GetMapping(path="/teste/find", produces="application/json")
	public ResponseEntity<List<UserExampleResponseDTO>> findAllUsers() throws JsonProcessingException;

	@ResponseBody
	@PostMapping(path="/teste/create", consumes="application/json", produces="application/json")
	ResponseEntity<byte[]> build(@RequestBody ProjectDto projectDto) throws IOException;
	
}
