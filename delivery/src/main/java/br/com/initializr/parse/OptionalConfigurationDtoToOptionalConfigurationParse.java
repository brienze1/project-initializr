package br.com.initializr.parse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import br.com.initializr.dto.OptionalConfigurationDto;
import br.com.initializr.entity.OptionalConfiguration;
import br.com.initializr.utils.MapperUtils;

@Component
public class OptionalConfigurationDtoToOptionalConfigurationParse {

	@Autowired
	private MapperUtils mapper;
	
	public OptionalConfiguration parse(OptionalConfigurationDto optionalConfigurationDto) {
		return mapper.map(optionalConfigurationDto, OptionalConfiguration.class);
	}
	
}
