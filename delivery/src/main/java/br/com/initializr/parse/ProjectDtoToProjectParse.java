package br.com.initializr.parse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import br.com.initializr.dto.ProjectDto;
import br.com.initializr.entity.Project;
import br.com.initializr.utils.MapperUtils;

@Component
public class ProjectDtoToProjectParse {
	
	@Autowired
	private MapperUtils mapper;
	
	@Autowired
	private OptionalConfigurationDtoToOptionalConfigurationParse optionalConfigurationDtoToOptionalConfigurationParse;
	
	public Project parse(ProjectDto projectDto) {
		Project project = mapper.map(projectDto, Project.class);
		if(project.getOptionalConfiguration() == null) {
			project.setOptionalConfiguration(optionalConfigurationDtoToOptionalConfigurationParse.parse(projectDto.getOptionalConfiguration()));
		}
		return project;
	}
	
}
