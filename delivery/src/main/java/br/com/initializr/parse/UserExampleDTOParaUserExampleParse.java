package br.com.initializr.parse;

import org.springframework.stereotype.Component;

import br.com.initializr.dto.UserExampleDTO;
import br.com.initializr.entity.UserExample;

@Component
public class UserExampleDTOParaUserExampleParse {
	
	public UserExample parse(UserExampleDTO userExampleDTO) {
		UserExample userExample = new UserExample();
		
		if(userExampleDTO != null) {
			userExample.setEmail(userExampleDTO.getEmail());
			userExample.setFirstName(userExampleDTO.getFirstName());
			userExample.setLastName(userExampleDTO.getLastName());
			userExample.setPassword(userExampleDTO.getPassword());
		}
		
		return userExample;
	}

}
