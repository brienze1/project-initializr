package br.com.initializr.dto;

import javax.validation.constraints.Email;

import com.fasterxml.jackson.annotation.JsonProperty;

import io.swagger.annotations.ApiModelProperty;

public class ProjectDto {

	@JsonProperty("first_name")
	private String firstName;
	
	@JsonProperty("last_name")
	private String lastName;

	@JsonProperty("email")
	@Email(regexp = ".@.\\..*", message = "Email should be valid")
	private String email;

	@JsonProperty("personal_website")
	private String personalWebsite;
	
	@JsonProperty("project_name")
	private String projectName;
	
	@JsonProperty("description")
	private String decription;
	
	@JsonProperty("base_package_name")
	private String basePackageName;
	
	@JsonProperty("base_project_url")
	private String baseProjectUrl;

	@JsonProperty("optional_configuration")
	private OptionalConfigurationDto optionalConfiguration;

	@ApiModelProperty(example = "firstNameExample")
	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	@ApiModelProperty(example = "lastNameExample")
	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	@ApiModelProperty(example = "firstNameExample.lastNameExample@email.com")
	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	@ApiModelProperty(example = "https://www.linkedin.com/in/firstNameExamplelastNameExample/")
	public String getPersonalWebsite() {
		return personalWebsite;
	}

	public void setPersonalWebsite(String personalWebsite) {
		this.personalWebsite = personalWebsite;
	}

	@ApiModelProperty(example = "Clean Architecture Template")
	public String getProjectName() {
		return projectName;
	}

	public void setProjectName(String projectName) {
		this.projectName = projectName;
	}

	@ApiModelProperty(example = "Template de projeto modular")
	public String getDecription() {
		return decription;
	}

	public void setDecription(String decription) {
		this.decription = decription;
	}

	@ApiModelProperty(example = "br.com.template")
	public String getBasePackageName() {
		return basePackageName;
	}

	public void setBasePackageName(String basePackageName) {
		this.basePackageName = basePackageName;
	}
	
	@ApiModelProperty(example = "https://gitlab.com/brienze1/clean-achitecture-template")
	public String getBaseProjectUrl() {
		return baseProjectUrl;
	}

	public void setBaseProjectUrl(String baseProjectUrl) {
		this.baseProjectUrl = baseProjectUrl;
	}

	public OptionalConfigurationDto getOptionalConfiguration() {
		return optionalConfiguration;
	}

	public void setOptionalConfiguration(OptionalConfigurationDto optionalConfiguration) {
		this.optionalConfiguration = optionalConfiguration;
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("ProjectDto [firstName=");
		builder.append(firstName);
		builder.append(", lastName=");
		builder.append(lastName);
		builder.append(", email=");
		builder.append(email);
		builder.append(", personalWebsite=");
		builder.append(personalWebsite);
		builder.append(", projectName=");
		builder.append(projectName);
		builder.append(", decription=");
		builder.append(decription);
		builder.append(", basePackageName=");
		builder.append(basePackageName);
		builder.append(", baseProjectUrl=");
		builder.append(baseProjectUrl);
		builder.append(", optionalConfiguration=");
		builder.append(optionalConfiguration);
		builder.append("]");
		return builder.toString();
	}

}
