package br.com.initializr.dto;

import com.fasterxml.jackson.annotation.JsonProperty;

import io.swagger.annotations.ApiModelProperty;

public class OptionalConfigurationDto {
	
	@JsonProperty("swagger")
	private boolean swagger;
	
	@JsonProperty("rest_template")
	private boolean restTemplate;
	
	@JsonProperty("data_base")
	private boolean dataBase;

	@ApiModelProperty(example = "true")
	public boolean getSwagger() {
		return swagger;
	}

	public void setSwagger(boolean swagger) {
		this.swagger = swagger;
	}

	@ApiModelProperty(example = "false")
	public boolean getRestTemplate() {
		return restTemplate;
	}

	public void setRestTemplate(boolean restTemplate) {
		this.restTemplate = restTemplate;
	}

	@ApiModelProperty(example = "true")
	public boolean getDataBase() {
		return dataBase;
	}

	public void setDataBase(boolean dataBase) {
		this.dataBase = dataBase;
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("OptionalConfigurationDto [swagger=");
		builder.append(swagger);
		builder.append(", restTemplate=");
		builder.append(restTemplate);
		builder.append(", dataBase=");
		builder.append(dataBase);
		builder.append("]");
		return builder.toString();
	}
	
}
