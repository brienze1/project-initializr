package br.com.initializr.entity;

public class Project {

	private String firstName;
	private String lastName;
	private String email;
	private String personalWebsite;
	private String projectName;
	private String decription;
	private String basePackageName;
	private String baseProjectUrl;
	private OptionalConfiguration optionalConfiguration;
	
	public String getFirstName() {
		return firstName;
	}
	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}
	public String getLastName() {
		return lastName;
	}
	public void setLastName(String lastName) {
		this.lastName = lastName;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public String getPersonalWebsite() {
		return personalWebsite;
	}
	public void setPersonalWebsite(String personalWebsite) {
		this.personalWebsite = personalWebsite;
	}
	public String getProjectName() {
		return projectName;
	}
	public void setProjectName(String projectName) {
		this.projectName = projectName;
	}
	public String getDecription() {
		return decription;
	}
	public void setDecription(String decription) {
		this.decription = decription;
	}
	public String getBasePackageName() {
		return basePackageName;
	}
	public void setBasePackageName(String basePackageName) {
		this.basePackageName = basePackageName;
	}
	public String getBaseProjectUrl() {
		return baseProjectUrl;
	}
	public void setBaseProjectUrl(String baseProjectUrl) {
		this.baseProjectUrl = baseProjectUrl;
	}
	public OptionalConfiguration getOptionalConfiguration() {
		return optionalConfiguration;
	}
	public void setOptionalConfiguration(OptionalConfiguration optionalConfiguration) {
		this.optionalConfiguration = optionalConfiguration;
	}
	
	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("Project [firstName=");
		builder.append(firstName);
		builder.append(", lastName=");
		builder.append(lastName);
		builder.append(", email=");
		builder.append(email);
		builder.append(", personalWebsite=");
		builder.append(personalWebsite);
		builder.append(", projectName=");
		builder.append(projectName);
		builder.append(", decription=");
		builder.append(decription);
		builder.append(", basePackageName=");
		builder.append(basePackageName);
		builder.append(", baseProjectUrl=");
		builder.append(baseProjectUrl);
		builder.append(", optionalConfiguration=");
		builder.append(optionalConfiguration);
		builder.append("]");
		return builder.toString();
	}
	
}
