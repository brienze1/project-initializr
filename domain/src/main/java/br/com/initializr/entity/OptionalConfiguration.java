package br.com.initializr.entity;

public class OptionalConfiguration {

	private boolean swagger;
	private boolean restTemplate;
	private boolean dataBase;
	
	public boolean getSwagger() {
		return swagger;
	}
	public void setSwagger(boolean swagger) {
		this.swagger = swagger;
	}
	public boolean getRestTemplate() {
		return restTemplate;
	}
	public void setRestTemplate(boolean restTemplate) {
		this.restTemplate = restTemplate;
	}
	public boolean getDataBase() {
		return dataBase;
	}
	public void setDataBase(boolean dataBase) {
		this.dataBase = dataBase;
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("OptionalConfiguration [swagger=");
		builder.append(swagger);
		builder.append(", restTemplate=");
		builder.append(restTemplate);
		builder.append(", dataBase=");
		builder.append(dataBase);
		builder.append("]");
		return builder.toString();
	}

}
