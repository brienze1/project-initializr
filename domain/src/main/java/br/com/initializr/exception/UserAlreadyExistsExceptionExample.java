package br.com.initializr.exception;

public class UserAlreadyExistsExceptionExample extends Throwable{

	private static final long serialVersionUID = -3788766278090345347L;
	
	 public UserAlreadyExistsExceptionExample(String errorMessage) {
	        super(errorMessage);
    }
	
}
