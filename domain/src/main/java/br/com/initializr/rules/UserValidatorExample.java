package br.com.initializr.rules;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import br.com.initializr.entity.UserExample;
import br.com.initializr.exception.UserAlreadyExistsExceptionExample;
import br.com.initializr.port.UserRepositoryAdapter;

@Component
public class UserValidatorExample {
	
	@Autowired
	private UserRepositoryAdapter repository;
	
	public void validateCreateUser(UserExample user) throws UserAlreadyExistsExceptionExample {
		
		if (repository.findByEmail(user.getEmail()).isPresent()) {
			throw new UserAlreadyExistsExceptionExample(user.getEmail());
		}
		
	}

}
