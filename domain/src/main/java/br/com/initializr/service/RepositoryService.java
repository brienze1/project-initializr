package br.com.initializr.service;

import java.io.File;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import br.com.initializr.entity.OptionalConfiguration;
import br.com.initializr.entity.Project;
import br.com.initializr.enums.ConfigEnum;
import br.com.initializr.port.FileUtilsAdapter;

@Component
public class RepositoryService {
	
	@Autowired
	private FileUtilsAdapter fileUtils;
	
	@Value("${project.builder.config.deletefiles.resttemplate:empty}")
	private String[] deleteFilesRestTemplate;
	
	@Value("${project.builder.config.cleanfiles.resttemplate:empty}")
	private String[] cleanFilesRestTemplate;
	
	@Value("${project.builder.config.deletefiles.swagger:empty}")
	private String[] deleteFilesSwagger;
	
	@Value("${project.builder.config.cleanfiles.swagger:empty}")
	private String[] cleanFilesSwagger;

	@Value("${project.builder.config.deletefiles.database:empty}")
	private String[] deleteFilesDataBase;
	
	@Value("${project.builder.config.cleanfiles.database:empty}")
	private String[] cleanFilesDataBase;
	
	@Value("${project.builder.config.deletefiles.database.else:empty}")
	private String[] deleteFilesDataBaseElse;
	
	@Value("${project.builder.config.cleanfiles.database.else:empty}")
	private String[] cleanFilesDataBaseElse;
	
	public void clean(File fileDir, Project project) {
		OptionalConfiguration config = project.getOptionalConfiguration();

		if (!config.getRestTemplate()) {
			fileUtils.clean(fileDir, cleanFilesRestTemplate, deleteFilesRestTemplate, Arrays.asList(ConfigEnum.START_REST_TEMPLATE.value()), Arrays.asList(ConfigEnum.END_REST_TEMPLATE.value()));
		}
		if (!config.getSwagger()) {
			fileUtils.clean(fileDir, cleanFilesSwagger, deleteFilesSwagger, Arrays.asList(ConfigEnum.START_SWAGGER.value()), Arrays.asList(ConfigEnum.END_SWAGGER.value()));
		}
		if (!config.getDataBase()) {
			fileUtils.clean(fileDir, cleanFilesDataBase, deleteFilesDataBase, Arrays.asList(ConfigEnum.START_DATA_BASE.value()), Arrays.asList(ConfigEnum.END_DATA_BASE.value()));
		} else {
			fileUtils.clean(fileDir, cleanFilesDataBaseElse, deleteFilesDataBaseElse, Arrays.asList(ConfigEnum.START_DATA_BASE_ELSE.value()), Arrays.asList(ConfigEnum.END_DATA_BASE_ELSE.value()));
		}

		Map<String, String> map = new HashMap<>();
		map.put("br.com.template", project.getBasePackageName());
		map.put("Template de projeto modular", project.getDecription());
		map.put("Template Clean Architecture para futuros projetos", project.getDecription());
		map.put("Template Clean Architecture", project.getProjectName());
		map.put("Luis", project.getFirstName());
		map.put("Brienze", project.getLastName());
		map.put("https://www.linkedin.com/in/luisbrienze/", project.getPersonalWebsite());
		map.put("lfbrienze@gmail.com", project.getEmail());
		
		for (String string : ConfigEnum.listAll()) {
			map.put(string, "");
		}
		
		fileUtils.replaceDefaultNames(fileDir, map);
	}

	public void fixFolders(File fileDir, Project project) {
		List<String> modules = new ArrayList<>();
		modules.add("application");
		modules.add("delivery");
		modules.add("domain");
		modules.add("integration");
		
		if(!project.getBasePackageName().equals("br.com.template")) {
			for (String module : modules) {
				File repositoryMain = new File(fileDir.getPath() + "\\" + module  + "\\src\\main\\java\\" + project.getBasePackageName().replaceAll("\\.", "\\\\"));
				File repositoryTest = new File(fileDir.getPath() + "\\" + module  + "\\src\\test\\java\\" + project.getBasePackageName().replaceAll("\\.", "\\\\"));
				
				fileUtils.copyFiles(new File(fileDir.getPath() + "\\" + module + "\\src\\main\\java\\br\\com\\template"), repositoryMain);
				fileUtils.copyFiles(new File(fileDir.getPath() + "\\" + module + "\\src\\test\\java\\br\\com\\template"), repositoryTest);
				
				fileUtils.delete(new File(fileDir.getPath() + "\\" + module + "\\src\\main\\java\\br\\com\\template"));
				fileUtils.delete(new File(fileDir.getPath() + "\\" + module + "\\src\\test\\java\\br\\com\\template"));
			}
		}
	}
	
}


















