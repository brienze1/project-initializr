package br.com.initializr.service;

import java.io.File;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import br.com.initializr.entity.Project;
import br.com.initializr.port.FileUtilsAdapter;
import br.com.initializr.port.GitWebServiceAdapter;
import br.com.initializr.port.ProjectBuilderServiceAdapter;
import br.com.initializr.port.ProjectRepositoryAdapter;
import br.com.initializr.port.ZipUtilityAdapter;

@Component
public class ProjectBuilderService implements ProjectBuilderServiceAdapter {
	
	@Autowired
	private ProjectRepositoryAdapter projectRepositoryAdapter;
	
	@Autowired
	private GitWebServiceAdapter gitWebService;
	
	@Autowired
	private FileUtilsAdapter fileUtils;
	
	@Autowired
	private RepositoryService repositoryService;
	
	@Autowired
	private ZipUtilityAdapter zipUtility;
	
	private static final String PATH_TO_REPO = System.getProperty("user.dir") + "\\template\\temp\\";
	private static final String PATH_TO_ZIP = System.getProperty("user.dir") + "\\template\\zip";
	private static final String ZIP = ".zip";

	@Override
	public File build(Project project) {
		projectRepositoryAdapter.save(project);
		
		File fileRepository = fileUtils.createRepository(PATH_TO_REPO + project.getProjectName().toLowerCase().replaceAll(" ", "-"));
		File zipRepository = fileUtils.createRepository(PATH_TO_ZIP);
		
		gitWebService.downloadBaseProject(project.getBaseProjectUrl(), fileRepository);
		
		repositoryService.clean(fileRepository, project);
		
		repositoryService.fixFolders(fileRepository, project);
		
		//TODO change default messages and names
		
		File zipFile = zipUtility.zip(fileRepository, zipRepository.getAbsolutePath() + "\\" + project.getProjectName().toLowerCase().replaceAll(" ", "-") + ZIP);
		
		fileUtils.delete(fileRepository);
		
		return zipFile;
	}

}
