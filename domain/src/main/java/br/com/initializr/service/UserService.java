package br.com.initializr.service;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import br.com.initializr.entity.UserExample;
import br.com.initializr.exception.UserAlreadyExistsExceptionExample;
import br.com.initializr.port.IdGeneratorAdapter;
import br.com.initializr.port.PasswordEncoderAdapter;
import br.com.initializr.port.UserRepositoryAdapter;
import br.com.initializr.port.UserServiceAdapter;
import br.com.initializr.rules.UserValidatorExample;

@Component
public class UserService implements UserServiceAdapter {

	@Autowired
	private UserValidatorExample userValidatorExample;
	
	@Autowired
	private UserRepositoryAdapter repository;
	
	@Autowired
	private PasswordEncoderAdapter passwordEncoder;
	
	@Autowired
	private IdGeneratorAdapter idGenerator;
	
	public String create(UserExample user) throws UserAlreadyExistsExceptionExample {
		userValidatorExample.validateCreateUser(user);
		
		UserExample userToSave = new UserExample();
		userToSave.setId(idGenerator.generate());
		userToSave.setEmail(user.getEmail());
		userToSave.setPassword(passwordEncoder.encode(user.getEmail()+user.getPassword()));
		userToSave.setLastName(user.getLastName());
		userToSave.setFirstName(user.getFirstName());
		
		repository.create(userToSave);
		
		return userToSave.getId();
	}
	
	public Optional<UserExample> findById(final String id){
		
		return repository.findById(id);
	}
	
	public List<UserExample> findAllUsers(){
		
		return repository.findAllUsers();
	}
	
	public Optional<UserExample> findByEmail(String email){
		
		return repository.findByEmail(email);
	}
	
}
