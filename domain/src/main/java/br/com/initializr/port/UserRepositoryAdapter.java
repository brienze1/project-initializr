package br.com.initializr.port;

import java.util.List;
import java.util.Optional;

import br.com.initializr.entity.UserExample;

public interface UserRepositoryAdapter {
	
	public Optional<UserExample> findById(String id);
	
	public List<UserExample> findAllUsers();

	public void create(UserExample user);

	public Optional<UserExample> findByEmail(String email);
	
}
