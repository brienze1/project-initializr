package br.com.initializr.port;

import java.io.File;

public interface ZipUtilityAdapter {

	File zip(File file, String destZipFile);
	
}
