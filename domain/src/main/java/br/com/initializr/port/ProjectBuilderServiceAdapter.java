package br.com.initializr.port;

import java.io.File;

import br.com.initializr.entity.Project;

public interface ProjectBuilderServiceAdapter {

	File build(Project project);
	
}
