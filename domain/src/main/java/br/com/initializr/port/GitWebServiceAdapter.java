package br.com.initializr.port;

import java.io.File;

public interface GitWebServiceAdapter {

	public void downloadBaseProject(String gitUrl, File repository);
	
}
