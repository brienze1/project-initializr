package br.com.initializr.port;

import java.io.File;
import java.util.List;
import java.util.Map;

public interface FileUtilsAdapter {

	File createRepository(String destination);

	void clean(File fileDir, String[] cleanFiles, String[] deleteFiles, List<String> start, List<String> stop);

	String[] listAll(File fileDir);
	
	void renameFolder(File source, String newName);
	
	void delete(File fileToDelete);

	String[] listAllRepositories(File fileRepository);

	void copyFiles(File file, File repository);

	void replaceDefaultNames(File fileDir, Map<String, String> map);

}
