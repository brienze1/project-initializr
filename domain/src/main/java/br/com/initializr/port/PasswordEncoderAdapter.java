package br.com.initializr.port;

public interface PasswordEncoderAdapter {

	String encode(String string);

}
