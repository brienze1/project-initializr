package br.com.initializr.port;

import br.com.initializr.entity.Project;

public interface ProjectRepositoryAdapter {

	void save(Project project);
	
}
