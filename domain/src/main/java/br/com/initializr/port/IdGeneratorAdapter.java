package br.com.initializr.port;

public interface IdGeneratorAdapter {

	String generate();

}
