package br.com.initializr.enums;

import java.util.ArrayList;
import java.util.List;

public enum ConfigEnum {
	
	START_REST_TEMPLATE("REST TEMPLATE CONFIG START"),
	END_REST_TEMPLATE("REST TEMPLATE CONFIG END"),
	START_SWAGGER("SWAGGER CONFIG START"),
	END_SWAGGER("SWAGGER CONFIG END"),
	START_DATA_BASE("DATA BASE CONFIG START"),
	END_DATA_BASE("DATA BASE CONFIG END"),
	START_DATA_BASE_ELSE("DATA BASE ELSE CONFIG START"),
	END_DATA_BASE_ELSE("DATA BASE ELSE CONFIG END");
	
    private String string;

    ConfigEnum(String string) {
        this.string = string;
    }

    public String value() {
        return string;
    }
    
    public static List<String> listAll() {
    	List<String> list = new ArrayList<>();

    	for (ConfigEnum enumValue : ConfigEnum.values()) {
			list.add(enumValue.value());
		}
    	
    	return list;
    }
    
}
